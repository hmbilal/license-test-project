<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $users = User::all();

        $pageTitle = 'All Users';
        $smallTitle = '(' . count($users) . ')';
        $breadcrumbs = [['text' => 'All Users']];
        $viewParams = [
            'users' => $users,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('user.index', $viewParams);
    }

    function LoginHistory($id)
    {
        $user = User::find($id);
        $histories = $user->servers->all();
        $pageTitle = 'Ip login history of ' . $user->name;
        $smallTitle = '(' . count($histories) . ')';
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'histories' => $histories,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('user.iphistory', $viewParams);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all()->pluck('label', 'id');
        $pageTitle = !empty($user) ? 'Edit ' . $user->username : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Edit User']];
        $viewParams = [
            'user' => $user,
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('user.edit', $viewParams);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email,' . $id,
            'password' => 'nullable|max:255|between:6,20',
        ];

        if (Auth::user()->isSuperUser()) {
            $rules['roles'] = 'required|array';
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->active = $request->input('active', 0);

            $password = $request->input('password');
            $roles = $request->input('roles', array());

            // change password
            if (!empty($password)) {
                $user->password = bcrypt($password);
            }

            if ($user->save()) {

                if ($roles) {
                    $user->roles()->sync($roles);
                }

                Session::flash('successMessage', 'User has been updated!');

                return redirect()->back();

            } else {
                return redirect()->back()
                    ->withErrors('Failed to update the user!')
                    ->withInput();
            }
        }

    }

    /**
     * Show the form for creating user.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->pluck('label', 'id');

        $pageTitle = 'Create User';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Create User']];
        $viewParams = [
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('user.create', $viewParams);
    }

    /**
     * Store the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email',
            'password' => 'required|max:255|between:6,20',
            'roles' => 'required|array',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $user = new User();

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->active = $request->input('active', 0);
            $password = $request->input('password');
            $roles = $request->input('roles', array());
            // password
            $user->password = bcrypt($password);

            if ($user->save()) {

                $user->roles()->sync($roles);

                Session::flash('successMessage', 'User has been created!');

                return redirect()->back();

            } else {
                return redirect()->back()
                    ->withErrors('Failed to create the user!')
                    ->withInput();
            }
        }

    }

    public function password()
    {

        $user = Auth()->user();
        $pageTitle = 'Change password of '.$user->name;
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Change Password']];
        $viewParams = [
            'user' => $user,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('user.password.index', $viewParams);
    }

    public function passwordupdate(Request $request, $id)
    {
        $user = User::find($id);
        $rules = [
            'password' => 'required|max:255|between:6,20|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $password = $request->input('password');
            if (!empty($password)) {
                $user->password = bcrypt($password);
                $user->save();
                Session::flash('successMessage', 'Your Password has changed successfully');
                return redirect()->back();
            }
        }
    }

    public function destroy($id)
    {
        $model = User::find($id);
        if ($model) {
            $result = $model->delete();
            if ($result) {
                Session::flash('successMessage', 'User has been deleted!');
                return redirect()->route('users');
            } else {
                return redirect()->back()
                    ->withErrors('Failed to delete user!')
                    ->withInput();
            }
        }
        return redirect()->back()
            ->withErrors('User not found!')
            ->withInput();
    }
}
