<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\Game;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $games = Game::all();

        $pageTitle = 'All Games';
        $smallTitle = '(' . count($games) . ')';
        $breadcrumbs = [['text' => 'All Games']];
        $viewParams = [
            'games' => $games,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('game.index', $viewParams);
    }

    /**
     * Show the form for editing the specified game.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = game::find($id);
        $pageTitle = !empty($game) ? 'Edit ' . $game->game : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Edit Game']];
        $viewParams = [
            'game' => $game,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('game.edit', $viewParams);
    }

    /**
     * Update the specified game in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $game = Game::find($id);
        $rules = [
            'game' => 'required|max:255|unique:games,game,' . $id,
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $game->game = $request->input('game');

            if ($game->save()) {

                Session::flash('successMessage', 'Game has been updated!');

                return redirect()->back();

            } else {
                return redirect()->back()
                    ->withErrors('Failed to update the Game!')
                    ->withInput();
            }
        }

    }

    /**
     * Show the form for creating game.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Create Game';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Create Game']];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('game.create', $viewParams);
    }

    /**
     * Store the specified game in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'game' => 'required|max:255|unique:games,game',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $game = new Game();

            $game->game = $request->input('game');

            if ($game->save()) {

                Session::flash('successMessage', 'Game has been created!');

                return redirect()->route('games');

            } else {
                return redirect()->back()
                    ->withErrors('Failed to create the Game!')
                    ->withInput();
            }
        }

    }

    public function destroy($id)
    {
        $model = Game::find($id);
        if ($model) {
            $result = $model->delete();
            if ($result) {
                Session::flash('successMessage', 'Game has been deleted!');
                return redirect()->route('games');
            } else {
                return redirect()->back()
                    ->withErrors('Failed to delete game!')
                    ->withInput();
            }
        }
        return redirect()->back()
            ->withErrors('Game not found!')
            ->withInput();
    }
}
