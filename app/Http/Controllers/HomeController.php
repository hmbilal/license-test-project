<?php

namespace App\Http\Controllers;

use App\Models\Server;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $iplogs = Server::with('user')->whereDate('created_at', Carbon::today())->get();
        $pageTitle = 'Home';
        $smallTitle = 'Home';
        $breadcrumbs = [['text' => 'Home']];
        $viewParams = [
            'iplogs' => $iplogs,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('home', $viewParams);
    }
}
