<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Server;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $user = User::where('email', $request->email);

        if ($user->exists()) {
            $user = $user->first();
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $server = new Server();
                $server->ip_address = $_SERVER['SERVER_ADDR'];
                $server->user_agent = $_SERVER['HTTP_USER_AGENT'];
                $server->user()->associate($user);
                $server->save();
                $user->last_login = Carbon::now();
                $user->save();
                return redirect()->intended('home');
            } else {
                throw ValidationException::withMessages(["Invalid / Wrong Password"]);
            }
        } else {
            throw ValidationException::withMessages(["User Not Found"]);
        }
    }
}
