<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Models\Role;
use App\Models\Permission;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $roles = Role::all();

        $pageTitle = 'All Roles';
        $smallTitle = '(' . count($roles) . ')';
        $breadcrumbs = [['text' => 'All Roles']];
        $viewParams = [
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('roles.index', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $permissions = Permission::all()->pluck('label', 'id');

        $pageTitle = 'Create Role';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Create Role']];
        $viewParams = [
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('roles.create', $viewParams);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|max:255|unique:roles,name',
            'label' => 'required|max:255',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $role = new Role;

            $name = $request->input('name');
            $label = $request->input('label');
            $permissions = $request->input('permissions', array());

            $role->name = str_replace(' ', '-', strtolower($name));
            $role->label = $label;

            if ($role->save()) {

                $role->permissions()->sync($permissions);

                Session::flash('successMessage', 'Role has been created!');

                return redirect()->back();
            } else {
                return redirect()->back()
                    ->withErrors('Failed to create role!')
                    ->withInput();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        $pageTitle = !empty($role) ? $role->label : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Role Details']];
        $viewParams = [
            'role' => $role,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('roles.show', $viewParams);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all()->pluck('label', 'id');

        $pageTitle = !empty($role) ? 'Edit ' . $role->name : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Edit Role']];
        $viewParams = [
            'role' => $role,
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('roles.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|alpha_dash|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $role = Role::find($id);

            $name = $request->input('name');
            $label = $request->input('label');
            $permissions = $request->input('permissions', array());

            $role->name = str_replace(' ', '-', strtolower($name));
            $role->label = $label;

            if ($role->save()) {

                $role->permissions()->sync($permissions);

                Session::flash('successMessage', 'Role has been updated!');

                return redirect()->back();
            } else {
                return redirect()->back()
                    ->withErrors('Failed to update the role!')
                    ->withInput();
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of permissions.
     *
     * @return \Illuminate\Http\Response
     */

    public function getRolePermissions($id)
    {
        $role = Role::find($id);
        $permissions = $role->permissions;

        $pageTitle = 'All Permissions of '.$role->label;
        $smallTitle = '(' . count($permissions) . ')';
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('roles.permissions', $viewParams);
    }

    public function getAllPermissions()
    {
        $permissions = Permission::all();

        $pageTitle = 'All Permissions';
        $smallTitle = '(' . count($permissions) . ')';
        $breadcrumbs = [['text' => 'All Permissions']];
        $viewParams = [
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('roles.permissions', $viewParams);
    }

    /**
     * Read permissions from all models and sync with db
     *
     * @return \Illuminate\Http\Response
     */
    public function syncPermissions()
    {

        if (Permission::syncPermissions()) {

            Session::flash('successMessage', 'Permissions synced!');

            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors('Failed to sync permissions!');
        }

    }
}
