<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\License;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $licenses = License::all();

        $pageTitle = 'All Licenses';
        $smallTitle = '(' . count($licenses) . ')';
        $breadcrumbs = [['text' => 'All Licenses']];
        $viewParams = [
            'licenses' => $licenses,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('license.index', $viewParams);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all()->pluck('label', 'id');
        $pageTitle = !empty($user) ? 'Edit ' . $user->username : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Edit User']];
        $viewParams = [
            'user' => $user,
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('user.edit', $viewParams);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email,' . $id,
            'password' => 'nullable|max:255|between:6,20',
        ];

        if (Auth::user()->isSuperUser()) {
            $rules['roles'] = 'required|array';
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->active = $request->input('active', 0);

            $password = $request->input('password');
            $roles = $request->input('roles', array());

            // change password
            if (!empty($password)) {
                $user->password = bcrypt($password);
            }

            if ($user->save()) {

                if ($roles) {
                    $user->roles()->sync($roles);
                }

                Session::flash('successMessage', 'User has been updated!');

                return redirect()->back();

            } else {
                return redirect()->back()
                    ->withErrors('Failed to update the user!')
                    ->withInput();
            }
        }

    }

    /**
     * Show the form for creating user.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $games = Game::all()->pluck('game', 'id');

        $pageTitle = 'Create License';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Create License']];
        $viewParams = [
            'games' => $games,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('license.create', $viewParams);
    }

    /**
     * Store the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'key' => 'required|max:255|unique:licenses,key',
            'game' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $license = new License();

            $license->key = $request->input('key');
            $license->note = $request->input('note');
            $license->ip = $request->input('ip');
            $license->duration = $request->input('duration');
            $license->end_at = $request->input('end_at');
            $license->active = $request->input('active', 0);
            $game = Game::find($request->input('game'));
            $license->game()->associate($game);
            $license->user()->associate(Auth::user());
            if ($license->save()) {

                Session::flash('successMessage', 'New License has been created!');
                return redirect()->route('licenses');

            } else {
                return redirect()->back()
                    ->withErrors('Failed to create the License!')
                    ->withInput();
            }
        }

    }

    public function destroy($id)
    {
        $model = License::find($id);
        if ($model) {
            $result = $model->delete();
            if ($result) {
                Session::flash('successMessage', 'License has been deleted!');
                return redirect()->route('licenses');
            } else {
                return redirect()->back()
                    ->withErrors('Failed to delete License!')
                    ->withInput();
            }
        }
        return redirect()->back()
            ->withErrors('License not found!')
            ->withInput();
    }

}
