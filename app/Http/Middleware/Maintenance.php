<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;

class Maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(\Illuminate\Support\Facades\Schema::hasTable('settings') && !Auth::check()) {
            $maintenance = Setting::where('name', 'maintenance')->first();

            if ((boolean)$maintenance->value) {
                return abort(503);
            }
        }

        return $next($request);
    }
}
