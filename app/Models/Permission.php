<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Support\Facades\DB;

class Permission extends Model implements Permissions
{

    protected $fillable = ['name', 'label'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_permission');
                    break;
                case 'create':
                case 'store':
                    return array('create_permission');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_permission');
                    break;
                case 'sync':
                    return array('sync_permission');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_permission',
            'create_permission',
            'edit_permission',
            'delete_permission',
            'sync_permission',
        );
    }

    /**
     * Read permissions from all models and update in db
     *
     */
    public static function syncPermissions()
    {

        $models = self::getModels(app_path() . "/Models", '\App\Models\\');

        $permissions = array();
        foreach ($models as $model) {
            if (method_exists($model, 'modulePermissions')) {
                $permissions = array_merge($permissions, $model::modulePermissions());
            }
        }

        $existingPermissions = self::all()->pluck('name')->toArray();

        $permissionsToBeDeleted = array_diff($existingPermissions, $permissions);
        $permissionsToBeAdded = array_diff($permissions, $existingPermissions);

        if (!empty($permissionsToBeDeleted)) {
            Permission::whereIn('name', $permissionsToBeDeleted)->delete();
        }

        foreach ($permissionsToBeAdded as $permission) {
            Permission::create([
                'name' => $permission,
                'label' => ucwords(str_replace('_', ' ', $permission)),
            ]);
        }

        return true;

    }

    /**
     * Get all model names with namespace from given path
     *
     * @param $path path to models directory
     * @param $namespace namespace for models
     * @return array model names with namespace
     */
    private static function getModels($path, $namespace)
    {
        $models = [];
        $results = scandir($path);

        foreach ($results as $result) {

            if ($result === '.' or $result === '..')
                continue;

            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $models = array_merge($models, self::getModels($filename));
            } else {
                $models[] = substr($namespace . $result, 0, -4);
            }
        }

        return $models;
    }
}
