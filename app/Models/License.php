<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\SoftDeletes;

class License extends Model implements Permissions
{

    use SoftDeletes;

    protected $table = 'licenses';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function game()
    {
        return $this->belongsTo(Game::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_license');
                    break;
                case 'create':
                case 'store':
                    return array('create_license');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_license');
                    break;
                case 'delete':
                    return array('delete_license');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_license',
            'create_license',
            'edit_license',
            'delete_license',
        );
    }
}
