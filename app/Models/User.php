<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Interfaces\Permissions;
use App\Models\Role;

class User extends Authenticatable implements Permissions
{

    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function licenses()
    {
        return $this->hasMany(License::class);
    }

    public function servers()
    {
        return $this->hasMany(Server::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function isSuperUser() {
        return $this->hasRole('admin');
    }

    public function assignRole($role)
    {
        $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles);

    }

    public function ability($permission = null)
    {
        return !is_null($permission) && $this->checkPermission($permission);
    }


    protected function checkPermission($permission)
    {
        $permissions = $this->getAllPermissionsFromAllRoles();

        if ($permissions === true) {
            return true;
        }

        $permissionArray = is_array($permission) ? $permission : [$permission];
        return count(array_intersect($permissions, $permissionArray));
    }

    protected function getAllPermissionsFromAllRoles()
    {
        $permissions = array();
        $roles = $this->roles->load('permissions');

        if (!$roles) {
            return true;
        }

        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions[] = $permission->toArray();
            }
        }

        return array_map('strtolower', array_unique(array_flatten(array_map(function ($permission) {
            return $permission['name'];
        }, $permissions))));

    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_user');
                    break;
                case 'create':
                case 'store':
                    return array('create_user');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_user');
                    break;
                case 'delete':
                    return array('delete_user');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_user',
            'create_user',
            'edit_user',
            'delete_user',
        );
    }
}
