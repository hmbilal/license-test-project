<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Server extends Model implements Permissions
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_server');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_server'
        );
    }
}
