<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Role extends Model implements Permissions
{
    protected $fillable = ['name', 'label'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_role');
                    break;
                case 'create':
                case 'store':
                    return array('create_role');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_role');
                    break;
                case 'delete':
                    return array('delete_role');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_role',
            'create_role',
            'edit_role',
            'delete_role',
        );
    }

}
