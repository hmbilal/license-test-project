<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model implements Permissions
{

    use SoftDeletes;

    protected $table = 'games';

    public function licenses()
    {
        return $this->hasMany(License::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_game');
                    break;
                case 'create':
                case 'store':
                    return array('create_game');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_game');
                    break;
                case 'delete':
                    return array('delete_game');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_game',
            'create_game',
            'edit_game',
            'delete_game',
        );
    }

}
