<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'name' => 'admin',
            'label' => 'admin',
        ]);
        Role::create([
            'name' => 'user',
            'label' => 'user',
        ]);
    }
}
