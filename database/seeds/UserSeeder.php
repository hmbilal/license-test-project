<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->delete();

        \App\Models\User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' =>  Hash::make('admin123'),
        ]);

        \App\Models\User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' =>  Hash::make('user123'),
        ]);
    }
}
