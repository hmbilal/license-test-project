<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_role')->delete();

        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            if ($permission->name == "view_license" ||
                $permission->name == "create_license" ||
                $permission->name == "edit_license" ||
                $permission->name == "delete_license") {

                DB::table('permission_role')->insert([
                    'permission_id' => $permission->id,
                    'role_id' => 1,
                ]);
                DB::table('permission_role')->insert([
                    'permission_id' => $permission->id,
                    'role_id' => 2,
                ]);

            } else {
                DB::table('permission_role')->insert([
                    'permission_id' => $permission->id,
                    'role_id' => 1,
                ]);
            }
        }
    }
}
