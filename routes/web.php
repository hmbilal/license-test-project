<?php

Route::group(['prefix' => '/'], function () {

    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    // access

    Route::get('/roles', ['as' => 'roles', 'uses' => 'RoleController@index',
        'permissions' => \App\Models\Role::modulePermissions(true, 'manage')]);
    Route::get('/roles/show/{id}', ['as' => 'roles.show', 'uses' => 'RoleController@show',
        'permissions' => \App\Models\Role::modulePermissions(true, 'show')]);
    Route::get('/roles/create', ['as' => 'roles.create', 'uses' => 'RoleController@create',
        'permissions' => \App\Models\Role::modulePermissions(true, 'create')]);
    Route::post('/roles/store', ['as' => 'roles.store', 'uses' => 'RoleController@store',
        'permissions' => \App\Models\Role::modulePermissions(true, 'store')]);
    Route::get('/roles/edit/{id}', ['as' => 'roles.edit', 'uses' => 'RoleController@edit',
        'permissions' => \App\Models\Role::modulePermissions(true, 'edit')]);
    Route::put('/roles/update/{id}', ['as' => 'roles.update', 'uses' => 'RoleController@update',
        'permissions' => \App\Models\Role::modulePermissions(true, 'update')]);

    // permissions

    Route::get('/role/permissions/{id}', ['as' => 'role.permissions', 'uses' => 'RoleController@getRolePermissions',
        'permissions' => \App\Models\Permission::modulePermissions(true, 'manage')]);
    Route::get('/role/permissions/sync', ['as' => 'role.permissions.sync', 'uses' => 'RoleController@syncPermissions',
        'permissions' => \App\Models\Permission::modulePermissions(true, 'sync')]);

    // user

    Route::get('/users', ['as' => 'users', 'uses' => 'UserController@index',
        'permissions' => \App\Models\User::modulePermissions(true, 'manage')]);
    Route::get('/user/create', ['as' => 'user.create', 'uses' => 'UserController@create',
        'permissions' => \App\Models\User::modulePermissions(true, 'create')]);
    Route::post('/user/store', ['as' => 'user.store', 'uses' => 'UserController@store',
        'permissions' => \App\Models\User::modulePermissions(true, 'store')]);
    Route::get('/user/edit/{id}', ['as' => 'user.edit', 'uses' => 'UserController@edit',
        'permissions' => \App\Models\User::modulePermissions(true, 'edit')]);
    Route::put('/user/update/{id}', ['as' => 'user.update', 'uses' => 'UserController@update',
        'permissions' => \App\Models\User::modulePermissions(true, 'update')]);
    Route::get('/user/destroy/{id}', ['as' => 'user.delete', 'uses' => 'UserController@destroy',
        'permissions' => \App\Models\User::modulePermissions(true, 'manage')]);
    Route::get('/user/login/history/{id}', ['as' => 'user.login.history', 'uses' => 'UserController@LoginHistory',
        'permissions' => \App\Models\Server::modulePermissions(true, 'manage')]);
    //games

    Route::get('/games', ['as' => 'games', 'uses' => 'GameController@index',
        'permissions' => \App\Models\Game::modulePermissions(true, 'manage')]);
    Route::get('/game/create', ['as' => 'game.create', 'uses' => 'GameController@create',
        'permissions' => \App\Models\Game::modulePermissions(true, 'create')]);
    Route::post('/game/store', ['as' => 'game.store', 'uses' => 'GameController@store',
        'permissions' => \App\Models\Game::modulePermissions(true, 'store')]);
    Route::get('/game/edit/{id}', ['as' => 'game.edit', 'uses' => 'GameController@edit',
        'permissions' => \App\Models\Game::modulePermissions(true, 'edit')]);
    Route::put('/game/update/{id}', ['as' => 'game.update', 'uses' => 'GameController@update',
        'permissions' => \App\Models\Game::modulePermissions(true, 'update')]);
    Route::get('/game/destroy/{id}', ['as' => 'game.delete', 'uses' => 'GameController@destroy',
        'permissions' => \App\Models\Game::modulePermissions(true, 'manage')]);

    //Licenses

    Route::get('/licenses', ['as' => 'licenses', 'uses' => 'LicenseController@index',
        'permissions' => \App\Models\License::modulePermissions(true, 'manage')]);
    Route::get('/license/create', ['as' => 'license.create', 'uses' => 'LicenseController@create',
        'permissions' => \App\Models\License::modulePermissions(true, 'create')]);
    Route::post('/license/store', ['as' => 'license.store', 'uses' => 'LicenseController@store',
        'permissions' => \App\Models\License::modulePermissions(true, 'store')]);
    Route::get('/license/edit/{id}', ['as' => 'license.edit', 'uses' => 'LicenseController@edit',
        'permissions' => \App\Models\License::modulePermissions(true, 'edit')]);
    Route::put('/license/update/{id}', ['as' => 'license.update', 'uses' => 'LicenseController@update',
        'permissions' => \App\Models\License::modulePermissions(true, 'update')]);
    Route::get('/license/destroy/{id}', ['as' => 'license.delete', 'uses' => 'LicenseController@destroy',
        'permissions' => \App\Models\License::modulePermissions(true, 'manage')]);

    Route::get('/user/password/', ['as' => 'user.password', 'uses' => 'UserController@password']);
    Route::put('/user/password/update/{id}', ['as' => 'user.password.update', 'uses' => 'UserController@passwordupdate']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
