@extends('layouts.muntazim')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    @if(!isset($post->id))
        <div class="alert alert-danger">Invalid Post!</div>
    @else

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">{{$post->name}}</h3>
                        <p class="text-muted text-center">{{$post->slug}}</p>
                        <p class="text-muted text-center">
                            @if($post->active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif
                        </p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Public</b>
                                @if($post->public)
                                    <span class="label label-success pull-right">Yes</span>
                                @else
                                    <span class="label label-danger pull-right">No</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <b>Tag</b>
                                @if($post->tag)
                                    <span class="label label-success pull-right">Yes</span>
                                @else
                                    <span class="label label-danger pull-right">No</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <b>Breaking News</b>
                                @if($post->breaking)
                                    <span class="label label-success pull-right">Yes</span>
                                @else
                                    <span class="label label-danger pull-right">No</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <b>Featured</b>
                                @if($post->featured)
                                    <span class="label label-success pull-right">Yes</span>
                                @else
                                    <span class="label label-danger pull-right">No</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <b>Order</b>
                                <span class="pull-right">{{$post->order}}</span>
                            </li>
                        </ul>

                        {!! link_to_route('muntazim.categories.edit', 'Edit', [$post->id], ['class' => 'btn btn-primary btn-block']) !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#content" data-toggle="tab">Content</a></li>
                        <li><a href="#meta" data-toggle="tab">Meta</a></li>
                        <li><a href="#image" data-toggle="tab">Image</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="active tab-pane" id="content">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3>Content</h3>
                                </div>
                                <div class="box-body">
                                    {{$post->content}}
                                </div>
                            </div>

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3>Excerpt</h3>
                                </div>
                                <div class="box-body">
                                    {{$post->excerpt}}
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->

                        <div class="tab-pane" id="meta">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3>Meta Description</h3>
                                </div>
                                <div class="box-body">
                                    {{$post->meta_description}}
                                </div>
                            </div>

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3>Meta Keywords</h3>
                                </div>
                                <div class="box-body">
                                    {{$post->meta_keywords}}
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->

                        <div class="tab-pane" id="image">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3>Image</h3>
                                </div>
                                @if($post->image)
                                    <div class="box-body">
                                        <img src="/uploads/posts/{{$post->image}}" class="img-responsive">
                                    </div>
                                @endif
                            </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    @endif

@stop