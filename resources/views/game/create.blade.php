@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="col-md-12">
        @include('partials.message')
    </div>
    <!-- form start -->
    {!! Form::open(['route' => 'game.store', 'files' => true] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('title', 'Game name') !!}
                        {!! Form::text('game', null, ['class' => 'form-control','required', 'id' => 'title' ]) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="box no-border">
            <div class="box-footer">
                {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
            </div>
        </div><!-- /.box -->
    </div>
    {!! Form::close() !!}
@stop

@section('pageScript')
@stop