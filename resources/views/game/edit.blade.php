@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    @include('partials.message')
    <!-- form start -->
    {!! Form::model($game, ['route' => ['game.update', $game->id], 'method' => 'PUT', 'files' => true] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('game', 'Game') !!}
                        {!! Form::text('game', null, ['class' => 'form-control', 'required', 'id' => 'title' ]) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="box no-border">
            <div class="box-footer">
                {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
            </div>
        </div><!-- /.box -->
    </div>
    {!! Form::close() !!}

@stop

@section('pageScript')
@stop