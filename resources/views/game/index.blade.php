@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-footer with-border text-right">
            <a href="{{route('game.create')}}" class="btn btn-sm btn-primary btn-flat">
                Create New Game
            </a>
        </div><!-- /.box-header -->
        <div class="box-body">
            @include('partials.message')
            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Game</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($games)>0)
                    @foreach($games as $game)
                        <tr>
                            <td>{{$game->id}}</td>
                            <td>{{$game->game}}</td>
                            <td>{{$game->created_at}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('game.edit', [$game->id])}}">Edit</a></li>
                                        @if(Auth::user()->ability('create_game'))
                                            <li>
                                                <a onclick="return confirm('Are you sure you want to delete this item?');"
                                                   href="{{route('game.delete', [$game->id])}}">Delete</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop