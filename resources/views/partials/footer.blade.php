<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <a target="_blank" href="https://bilzit.com/">Developed by Bilzit</a>
    </div>
    <strong class="text-center">Copyright © {{date('Y')}}</strong> All rights reserved.
</footer>