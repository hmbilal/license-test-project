<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="{{route('home', ['slug' => Auth::user()->name])}}"
                   target="_blank">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="{{ route('home') }}">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            @if(Auth::user()->ability('create_user'))
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Users</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url() == url('users')) {{ 'active' }} @endif">
                            <a href="{{route('users')}}"><i class="fa fa-circle-o"></i> All Users</a></li>
                        <li class="@if(Request::url() == url('user/create')) {{ 'active' }} @endif">
                            <a href="{{route('user.create')}}"><i class="fa fa-circle-o"></i> New User</a></li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->ability('create_role'))
                <li class="treeview active">
                    <a href="{{route('roles')}}">
                        <i class="fa fa-lock"></i> <span>Access</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url() == url('roles')) {{ 'active' }} @endif">
                            <a href="{{route('roles')}}"><i class="fa fa-circle-o"></i> All Roles</a>
                        </li>
                        <li class="@if(Request::url() == url('roles/create')) {{ 'active' }} @endif">
                            <a href="{{route('roles.create')}}"><i class="fa fa-circle-o"></i> New Role</a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->ability('create_game'))
                <li class="active">
                    <a href="{{route('games')}}">
                        <i class="fa fa-lock"></i> <span>Games</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->ability('create_license'))
                <li class="active">
                    <a href="{{route('licenses')}}">
                        <i class="fa fa-lock"></i> <span>Licenses</span>
                    </a>
                </li>
            @endif
            <li class="active">
                <a href="{{route('user.password')}}">
                    <i class="fa fa-lock"></i> <span>Change Password</span>
                </a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>