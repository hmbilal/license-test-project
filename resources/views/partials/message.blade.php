<div class="row">
    <div class="col-md-12">
        @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))
            <div class="callout callout-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p>{{ $successMessage }}</p>
            </div>
        @endif
    </div>
</div>