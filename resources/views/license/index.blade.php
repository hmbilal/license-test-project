@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-footer with-border">
            <div class="pull-right">
                <a href="{{route('license.create')}}" class="btn btn-sm btn-primary btn-flat">
                    Create New License
                </a>
            </div>
            <div class="clear"></div>
        </div><!-- /.box-header -->
        <div class="box-body">
            @include('partials.message')
            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>key</th>
                    <th>Ip</th>
                    <th>Duration</th>
                    <th>Note</th>
                    <th>Game</th>
                    <th>User</th>
                    <th>Created At</th>
                    <th>End Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($licenses)>0)
                    @foreach($licenses as $license)
                        <tr>
                            <td>{{$license->id}}</td>
                            <td>{{$license->key}}</td>
                            <td>{{$license->ip}}</td>
                            <td>{{$license->duration}}</td>
                            <td>{{$license->note}}</td>
                            <td>{{$license->game->game}}</td>
                            <td>{{$license->user->name}}</td>
                            <td>{{$license->created_at}}</td>
                            <td>{{$license->end_at}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('license.edit', [$license->id])}}">Edit</a></li>
                                        <li><a onclick="return confirm('Are you sure you want to delete this item?');"
                                               href="{{route('license.delete', [$license->id])}}">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="15" class="text-center">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop
