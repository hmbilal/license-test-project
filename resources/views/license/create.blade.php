@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('head')
@stop

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    @include('partials.message')
    {!! Form::open(['route' => ['license.store'], 'method' => 'POST', 'files' => true] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('game', 'Game') !!}
                        {!! Form::select('game', $games, null, array('id' => 'game',
                        'class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('key', 'Key') !!}
                        {!! Form::text('key', null, ['class' => 'form-control', 'placeholder' => 'Enter Key', 'required', 'autofocus', 'id' => 'key' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ip', 'Ip Address') !!}
                        {!! Form::text('ip',$_SERVER['SERVER_ADDR'], ['class' => 'form-control', 'placeholder' => 'Ip Address', 'required', 'id' => 'ip' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('duration', 'Duration') !!}
                        {!! Form::text('duration', null, ['class' => 'form-control', 'placeholder' => 'Enter Duration', 'required', 'autofocus', 'id' => 'key' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('note', 'Note') !!}
                        {!! Form::textarea('note', null, ['class' => 'form-control','rows' => 10, 'id' => 'note' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('end_at', 'End Date') !!}
                        {!! Form::text('end_at', null, ['class' => 'form-control form_datetime', 'placeholder' => 'Enter End date', 'autofocus' ]) !!}
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('active', 1) !!}
                            Active
                        </label>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>  <!-- row -->
    <div class="row">
        <div class="box no-border">
            <div class="box-footer">
                {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
            </div>
        </div><!-- /.box -->
    </div>

    {!! Form::close() !!}
    {{--</form>--}}
@stop

@section('pageScript')
@stop