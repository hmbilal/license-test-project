@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')
@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    @include('partials.message')
    {!! Form::model($user, ['route' => ['user.password.update', $user->id], 'method' => 'PUT'] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('password', 'Password') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password',
                        'value' => '', 'id' => 'password' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Password Confirmed') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password',
                        'value' => '', 'id' => 'password_confirmation' ]) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>  <!-- row -->
    <div class="row">
        <div class="box no-border">
            <div class="box-footer text-right">
                {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
            </div>
        </div><!-- /.box -->
    </div>
    {!! Form::close() !!}
    {{--</form>--}}

@stop