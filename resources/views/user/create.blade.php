@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('head')
    <link href="/vendor/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
@stop

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    @include('partials.message')
    {!! Form::open(['route' => ['user.store'], 'method' => 'POST', 'files' => true] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('inputName', 'Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Display Name', 'required', 'autofocus', 'id' => 'inputName' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputEmail', 'Email') !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required', 'id' => 'inputEmail' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputPassword', 'Password') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required',
                        'value' => '', 'id' => 'inputPassword' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('roles', 'Roles') !!}
                        {!! Form::select('roles[]', $roles, null, array('id' => 'roles',
                        'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true', 'data-selected-text-format' => 'count > 3',
                        'data-size' => '10', 'data-actions-box' => 'true', 'multiple' => 'multiple')) !!}
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('active', 1) !!}
                            Active
                        </label>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>  <!-- row -->
    <div class="row">
        <div class="box no-border">
            <div class="box-footer">
                {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
                {!! link_to_route('users', 'All Users', [], ['class' => 'btn btn-primary pull-right']) !!}
            </div>
        </div><!-- /.box -->
    </div>

    {!! Form::close() !!}
    {{--</form>--}}

@stop

@section('pageScript')
    <script type="text/javascript" src="/vendor/bootstrap-select/bootstrap-select.min.js"></script>
@stop