@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-body">
            @include('partials.message')
            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Ip Address</th>
                    <th>User Agent</th>
                    <th>Login Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($histories)>0)
                    @foreach($histories as $history)
                        <tr>
                            <td>{{$history->ip_address}}</td>
                            <td>{{$history->user_agent}}</td>
                            <td>{{$history->created_at}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="12">No History Found....</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop