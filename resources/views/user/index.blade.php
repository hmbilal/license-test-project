@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-footer with-border text-right">
            {!! link_to_route('user.create', 'Create New User', [], ['class' => 'btn btn-sm btn-primary btn-flat']) !!}
        </div><!-- /.box-header -->
        <div class="box-body">
            @include('partials.message')
            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Roles</th>
                    <th>Date Created</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if($user->active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">InActive</span>
                            @endif
                        </td>
                        <td>
                            @if ($user->roles)
                                <ul>
                                    @foreach($user->roles as $role)
                                        <li>{{$role->label}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </td>
                        <td>{{$user->created_at}}</td>

                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{!! link_to_route('user.edit', 'Edit', [$user->id]) !!}</li>
                                    @if(Auth::user()->ability('delete_user'))
                                        <li><a onclick="return confirm('Are you sure you want to delete this item?');"
                                               href="{{route('user.delete', [$user->id])}}">Delete</a></li>
                                    @endif
                                    @if(Auth::user()->ability('view_server'))
                                        <li><a href="{{route('user.login.history', [$user->id])}}">Login History</a></li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop