@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="box text-center box-body p-5">
        <h3>Welcome Back {{Auth::user()->name}}</h3>
    </div>
    @if(Auth::user()->isSuperUser())
        <div class="box box-info">
            <div class="box-header text-center">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Registered Users</span>
                                <span class="info-box-number">{{\App\Models\User::count()}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Games</span>
                                <span class="info-box-number">{{\App\Models\Game::count()}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-life-ring"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Licenses</span>
                                <span class="info-box-number">{{\App\Models\License::count()}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Logs History ({{\Carbon\Carbon::today()}})</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Email</th>
                            <th>Ip</th>
                            <th>Agent</th>
                            <th>Last Login</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($iplogs)>0)
                            @foreach($iplogs as $iplog)
                                <tr>
                                    <td>{{$iplog->user->name}}</td>
                                    <td>{{$iplog->user->email}}</td>
                                    <td>{{$iplog->ip_address}}</td>
                                    <td>{{$iplog->user_agent}}</td>
                                    <td>{{\Carbon\Carbon::parse($iplog->user->last_login)->diffForHumans()}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="12" class="text-center">No Logs Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
    @endif
@endsection
