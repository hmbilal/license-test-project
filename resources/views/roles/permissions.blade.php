@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('head')
    <link href="/vendor/datatables/dataTables.bootstrap.css" rel="stylesheet">
@stop

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-body">

            @include('partials.message')

            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->label}}</td>
                        <td>{{$permission->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop

@section('pageScript')
    <script type="text/javascript" src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/vendor/datatables/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#accessListTbl').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "columnDefs": [ { "orderable": false, "targets": [1, 2] } ]
            });
        });
    </script>
@stop
