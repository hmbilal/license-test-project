@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    @if(empty($role))
        <div class="alert alert-danger">Invalid Role</div>
    @else

        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">{{$role->name}}</h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Label</b> <span class="pull-right">{{$role->label}}</span>
                            </li>

                            <li class="list-group-item">
                                <b>Created</b> <span class="pull-right">{{$role->created_at}}</span>
                            </li>

                        </ul>

                        {!! link_to_route('roles.edit', 'Edit', [$role->id], ['class' => 'btn btn-primary btn-block']) !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#permission" data-toggle="tab">Permissions</a></li>
                        <li><a href="#users" data-toggle="tab">Users</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="permission">

                            <div class="box">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Label</th>
                                        </tr>
                                        @foreach($role->permissions as $permission)
                                            <tr>
                                                <td>1.</td>
                                                <td>{{$permission->name}}</td>
                                                <td>{{$permission->label}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="users">

                            <div class="box">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                        </tr>
                                        @foreach($role->users as $user)
                                            <tr>
                                                <td>1.</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    @endif
@stop