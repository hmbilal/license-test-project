@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('head')
    <link href="/vendor/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
@stop

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <!-- general form elements -->
    <div class="box box-primary">
        <!-- form start -->
        {!! Form::open(['route' => ['roles.store'], 'method' => 'POST']) !!}
        {!! csrf_field() !!}
        <div class="box-body">

            @include('partials.message')
            <div class="form-group">
                {!! Form::label('inputName', 'Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required', 'autofocus', 'id' => 'inputName' ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputLabel', 'Label') !!}
                {!! Form::text('label', null, ['class' => 'form-control', 'placeholder' => 'label', 'required', 'id' => 'inputLabel' ]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('permissions', 'Permissions') !!}
                {!! Form::select('permissions[]', $permissions, null, array('id' => 'permissions',
                'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true', 'data-selected-text-format' => 'count > 3',
                'data-size' => '10', 'data-actions-box' => 'true', 'multiple' => 'multiple')) !!}
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit('Save!', array('class' => 'btn btn-primary')) !!}
            {!! link_to_route('roles', 'All Roles', [], ['class' => 'btn btn-primary pull-right']) !!}
        </div>
        {!! Form::close() !!}
        {{--</form>--}}
    </div><!-- /.box -->
@stop

@section('pageScript')
    <script type="text/javascript" src="/vendor/bootstrap-select/bootstrap-select.min.js"></script>
@stop