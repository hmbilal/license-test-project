@extends('layouts.dashbaord')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-footer with-border text-right">
            {!! link_to_route('roles.create', 'Create New Role', [], ['class' => 'btn btn-sm btn-primary btn-flat']) !!}
        </div><!-- /.box-header -->
        <div class="box-body">

            @include('partials.message')

            <table id="accessListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Date Created</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->label}}</td>
                        <td>{{$role->created_at}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{!! link_to_route('roles.show', 'View', [$role->id]) !!}</li>
                                    <li>{!! link_to_route('roles.edit', 'Edit', [$role->id]) !!}</li>
                                    <li>{!! link_to_route('role.permissions', 'Permissions', [$role->id]) !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop

@section('pageScript')
@stop
