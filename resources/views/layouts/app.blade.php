<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css') }}">
    <style>
        .login-box, .register-box {
            width: 31%;
            margin: 7% auto;
        }
        .register-box{
            width: 57%;
        }
        .login-box-body, .register-box-body {
            background: #ffffff3d;
            padding: 54px;
            border-top: 0;
            color: white;
        }
        .login-box-msg, .register-box-msg {
            margin: 0;
            text-align: center;
            padding: 0 20px 20px 20px;
            font-size: 23px;
        }
    </style>
</head>
<body class="hold-transition login-page"
      style="background-image: url(http://mkt.bilzit.local/svg/404.svg);background-repeat: no-repeat;background-size:cover;">
<main class="py-4">
    @yield('content')
</main>
</body>
</html>
