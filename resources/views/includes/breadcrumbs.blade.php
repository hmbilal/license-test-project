<section class="content-header">
    @if(isset($pageTitle) && $pageTitle != '')
        <h1>
            {{$pageTitle}}

            @if(isset($smallTitle) && $smallTitle != '')
                <small>{{$smallTitle}}</small>
            @endif
        </h1>
    @endif

    @if(isset($breadcrumbs))
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            @foreach($breadcrumbs as $leaf)
                @if(!empty($leaf['url']))
                    <li><a href="{{$leaf['url']}}">{{$leaf['text']}}</a></li>
                @else
                    <li class="active">{{$leaf['text']}}</li>
                @endif
            @endforeach
        </ol>
    @endif
</section>